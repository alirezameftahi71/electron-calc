window.addEventListener('DOMContentLoaded', () => {
    new ArmCalc({
        selector: document.getElementById('calculator'),
        style: {
            bgColor: 'black'
        }
    });
});
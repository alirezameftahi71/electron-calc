class ArmCalc {
    constructor(config) {
        this._initConfig(config);
        this._initUI();
        this._init();
        this.clear();
    }

    _initConfig(config) {
        this.selector = typeof config.selector.get === 'function' ? config.selector.get(0) : config.selector;
        this.previousOperandTextElement;
        this.currentOperandTextElement;
        this.currentOperand;
        this.previousOperand;
        this.operation;
        this.updateConfig(config);
    }

    _init() {
        this.previousOperandTextElement = this.selector.querySelector('[data-previous-operand]');
        this.currentOperandTextElement = this.selector.querySelector('[data-current-operand]');
        this.element = this.selector.querySelector('.arm-calc');
        if (this.config.style && this.config.style.bgColor) {
            this.element.style.setProperty('--bg-color', this.config.style.bgColor);
        }
        this.selector.querySelectorAll('[data-number]').forEach(button => {
            button.addEventListener('click', () => {
                this.appendNumber(button.innerText);
                this.updateDisplay();
            });
        });

        this.selector.querySelectorAll('[data-operation]').forEach(button => {
            button.addEventListener('click', () => {
                this.chooseOperation(button.innerText);
                this.updateDisplay();
            });
        });

        this.selector.querySelector('[data-equals]').addEventListener('click', button => {
            this.compute();
            this.updateDisplay();
        });

        this.selector.querySelector('[data-all-clear]').addEventListener('click', button => {
            this.clear();
            this.updateDisplay();
        });

        this.selector.querySelector('[data-delete]').addEventListener('click', button => {
            this.delete();
            this.updateDisplay();
        });
    }

    _initUI() {
        this.selector.innerHTML = `
            <div class="arm-calc">
                <div class="output">
                    <div data-previous-operand class="previous-operand"></div>
                    <div data-current-operand class="current-operand"></div>
                </div>
                <button data-all-clear class="span-two">AC</button>
                <button data-delete>DEL</button>
                <button data-operation>÷</button>
                <button data-number>1</button>
                <button data-number>2</button>
                <button data-number>3</button>
                <button data-operation>×</button>
                <button data-number>4</button>
                <button data-number>5</button>
                <button data-number>6</button>
                <button data-operation>+</button>
                <button data-number>7</button>
                <button data-number>8</button>
                <button data-number>9</button>
                <button data-operation>-</button>
                <button data-number>.</button>
                <button data-number>0</button>
                <button data-equals class="span-two">=</button>
            </div>`;
    }

    updateConfig(config) {
        this.config = config;
    }

    clear() {
        this.currentOperand = '';
        this.previousOperand = '';
        this.operation = undefined;
    }

    delete() {
        this.currentOperand = this.currentOperand.toString().slice(0, -1);
    }

    appendNumber(number) {
        if (number === '.' && this.currentOperand.toString().includes('.')) {
            return;
        }
        this.currentOperand = `${this.currentOperand}${number}`;
    }

    chooseOperation(operation) {
        if (this.currentOperand.toString() === '') return;
        if (this.previousOperand.toString() !== '') {
            this.compute();
        }
        this.operation = operation;
        this.previousOperand = this.currentOperand;
        this.currentOperand = '';
    }

    compute() {
        let computation;
        const prev = parseFloat(this.previousOperand);
        const current = parseFloat(this.currentOperand);
        if (isNaN(prev) || isNaN(current)) return;
        switch (this.operation) {
            case '+':
                computation = prev + current;
                break;
            case '-':
                computation = prev - current;
                break;
            case '×':
                computation = prev * current;
                break;
            case '÷':
                computation = prev / current;
                break;
            default:
                break;
        }
        this.currentOperand = computation;
        this.operation = undefined;
        this.previousOperand = '';
    }

    updateDisplay() {
        this.currentOperandTextElement.innerText = this.numberWithCommas(this.currentOperand);
        if (this.operation != null) {
            this.previousOperandTextElement.innerText =
                `${this.numberWithCommas(this.previousOperand)} ${this.operation}`;
        } else {
            this.previousOperandTextElement.innerText = '';
        }
    }

    numberWithCommas(number) {
        const parts = number.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
}

if (typeof jQuery === 'function') {
    $.fn.armCalc = function (config) {
        const _config = $.extend({
            selector: this
        }, config);
        return new ArmCalc(_config);
    };
}